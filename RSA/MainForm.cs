﻿/*
 * Created by SharpDevelop.
 * User: Eucliwood
 * Date: 29-Jul-17
 * Time: 23:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using System.Security.Cryptography;
using System.Text;

namespace RSA
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			UnicodeEncoding ByteConverter = new UnicodeEncoding();
			 byte[] encryptedData;
			 byte[] dataToEncrypt = ByteConverter.GetBytes("สวัสดีครัช");
             
              using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            { 
              	encryptedData = RSAEncrypt(dataToEncrypt,RSA.ExportParameters(false) , false);
				PublicKey_val.Text = RSA.ToXmlString(false);
                PrivateKey_val.Text = RSA.ToXmlString(true);
                Cipher_val.Text =  Convert.ToBase64String(encryptedData);
            }
           
	}
		
    public byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
    {
        try
        {
            byte[] encryptedData;
            //Create a new instance of RSACryptoServiceProvider.
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {

                //Import the RSA Key information. This only needs
                //toinclude the public key information.
                RSA.ImportParameters(RSAKeyInfo);

                //Encrypt the passed byte array and specify OAEP padding.  
                //OAEP padding is only available on Microsoft Windows XP or
                //later.  
                encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
            }
            return encryptedData;
        }
        //Catch and display a CryptographicException  
        //to the console.
        catch (CryptographicException e)
        {
            Console.WriteLine(e.Message);

            return null;
        }

    }
    public byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
    {
        try
        {
            byte[] decryptedData;
            //Create a new instance of RSACryptoServiceProvider.
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                //Import the RSA Key information. This needs
                //to include the private key information.
                RSA.ImportParameters(RSAKeyInfo);

                //Decrypt the passed byte array and specify OAEP padding.  
                //OAEP padding is only available on Microsoft Windows XP or
                //later.  
                decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
            }
             return decryptedData;
        }
         catch (CryptographicException e)
        {
            MessageBox.Show(e.ToString());

            return null;
        }
    }
		void Button1Click(object sender, EventArgs e)
		{
			byte[] decryptedData;
			UnicodeEncoding ByteConverter = new UnicodeEncoding();
			
			using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            { 
				byte[] cipher = Convert.FromBase64String(Cipher_val.Text);
				RSA.FromXmlString(PrivateKey_val.Text);
			   	decryptedData = RSADecrypt(cipher, RSA.ExportParameters(true), false);
			    plaintextval.Text = ByteConverter.GetString(decryptedData);			    
			}
		}
	}
}