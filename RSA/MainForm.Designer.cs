﻿/*
 * Created by SharpDevelop.
 * User: Eucliwood
 * Date: 29-Jul-17
 * Time: 23:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace RSA
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox PrivateKey_val;
		private System.Windows.Forms.TextBox PublicKey_val;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Cipher_val;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox plaintextval;
		private System.Windows.Forms.Button button1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.PrivateKey_val = new System.Windows.Forms.TextBox();
			this.PublicKey_val = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.Cipher_val = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.plaintextval = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// PrivateKey_val
			// 
			this.PrivateKey_val.Location = new System.Drawing.Point(135, 12);
			this.PrivateKey_val.Multiline = true;
			this.PrivateKey_val.Name = "PrivateKey_val";
			this.PrivateKey_val.Size = new System.Drawing.Size(557, 110);
			this.PrivateKey_val.TabIndex = 0;
			// 
			// PublicKey_val
			// 
			this.PublicKey_val.Location = new System.Drawing.Point(135, 147);
			this.PublicKey_val.Multiline = true;
			this.PublicKey_val.Name = "PublicKey_val";
			this.PublicKey_val.Size = new System.Drawing.Size(557, 95);
			this.PublicKey_val.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
			this.label1.Location = new System.Drawing.Point(24, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(105, 37);
			this.label1.TabIndex = 2;
			this.label1.Text = "PrivateKey";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
			this.label2.Location = new System.Drawing.Point(12, 152);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(105, 36);
			this.label2.TabIndex = 3;
			this.label2.Text = "PublicKey";
			// 
			// Cipher_val
			// 
			this.Cipher_val.Location = new System.Drawing.Point(135, 265);
			this.Cipher_val.Multiline = true;
			this.Cipher_val.Name = "Cipher_val";
			this.Cipher_val.Size = new System.Drawing.Size(557, 75);
			this.Cipher_val.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
			this.label4.Location = new System.Drawing.Point(12, 260);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(105, 43);
			this.label4.TabIndex = 7;
			this.label4.Text = "CipherText";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
			this.label3.Location = new System.Drawing.Point(24, 422);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 36);
			this.label3.TabIndex = 8;
			this.label3.Text = "PlainText";
			// 
			// plaintextval
			// 
			this.plaintextval.Location = new System.Drawing.Point(135, 422);
			this.plaintextval.Multiline = true;
			this.plaintextval.Name = "plaintextval";
			this.plaintextval.Size = new System.Drawing.Size(557, 75);
			this.plaintextval.TabIndex = 9;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(747, 435);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 10;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(870, 552);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.plaintextval);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.Cipher_val);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.PublicKey_val);
			this.Controls.Add(this.PrivateKey_val);
			this.Name = "MainForm";
			this.Text = "RSA";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
